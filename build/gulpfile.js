/**
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */
var gulp       = require('gulp');
var extension  = require('./package.json');
var argv       = require('yargs').argv;
var requireDir = require('require-dir');
var zip        = require('gulp-zip');
var xml2js     = require('xml2js');
var fs         = require('fs');

var parser = new xml2js.Parser();

var config      = require("./gulp-config.json");

var jgulp   = requireDir('./node_modules/joomla-gulp', {recurse: true});

var dir = requireDir('./jgulp', {recurse: true});

gulp.task('release', ['release:aesir_sync']);

// Override of the release script
gulp.task('release:aesir_sync', [], function (cb) {
    fs.readFile( '../extensions/aesir_sync.xml', function(err, data) {
        parser.parseString(data, function (err, result) {
            var version = result.extension.version[0];


            var fileName = argv.skipVersion ? 'aesir_sync.zip' : 'aesir_sync-v' + version + '.zip';
            console.log(config.release_dir);

            return gulp.src([
                '../extensions/**/*',
                '../extensions/*(install.php|aesir_sync.xml)',
                './*(LICENSE.txt)'
            ])
                .pipe(zip(fileName))
                .pipe(gulp.dest(config.release_dir))
                .on('end', cb);
        });
    });
});