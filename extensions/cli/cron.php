<?php
/**
 * @package     AesirSync.Cli
 * @subpackage  Sync
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later, see LICENSE.
 */


// Initialize Joomla framework
require_once __DIR__ . '/joomla_framework.php';

JLoader::import('sync', JPATH_ADMINISTRATOR . '/components/com_aesir_sync/classes');

/**
 * Synchronize cli application.
 *
 * @package     AesirSync.Cli
 * @subpackage  Sync
 * @since       1.0
 */
class Cron extends JApplicationCli
{

	function doExecute() {
		JFactory::$session = JFactory::getSession();
		JFactory::$session->set('user',JUser::getInstance(892));

		$app = JFactory::getApplication('site');
		$app->input->set('option', 'com_aesir_sync');
		JPluginHelper::importPlugin('system');
		JFactory::getApplication('site')->triggerEvent('onAfterInitialise');

		new Sync();
		print "ok";
	}

}

JApplicationCli::getInstance('Cron')->execute();
