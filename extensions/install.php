<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Install
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die;

// Find redCORE installer to use it as base system
if (!class_exists('Com_RedcoreInstallerScript'))
{
	$searchPaths = array(
		// Install
		dirname(__FILE__) . '/redCORE/extensions',
		// Discover install
		JPATH_ADMINISTRATOR . '/components/com_redcore'
	);

	if ($redcoreInstaller = JPath::find($searchPaths, 'install.php'))
	{
		require_once $redcoreInstaller;
	}
}

/**
 * Custom installation of Aesir Sync component
 *
 * @package     Aesir_Sync.Backend
 * @subpackage  Install
 * @since       1.0.0
 */
class Com_Aesir_SyncInstallerScript extends Com_RedcoreInstallerScript
{
	/**
	 * Method to run after an install/update/uninstall method
	 *
	 * @param   object  $type    type of change (install, update or discover_install)
	 * @param   object  $parent  class calling this method
	 *
	 * @return  boolean
	 */
	public function postflight($type, $parent)
	{
		if ($type == 'install' || $type == 'discover_install' || $type == 'update')
		{
			return $this->postProcessCron();
		}

		return true;
	}

	/**
	 * Post process and update full cron file
	 *
	 * @return bool
	 */
	public function postProcessCron()
	{

		return true;
	}
}
