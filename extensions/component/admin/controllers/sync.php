<?php
/**
 * @package     Aesir_Sync.Frontend
 * @subpackage  Controller.Crons
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

JLoader::import('sync', JPATH_ADMINISTRATOR . '/components/com_aesir_sync/classes');
JLoader::import('xmlimport', JPATH_ADMINISTRATOR . '/components/com_aesir_sync/classes');

defined('_JEXEC') or die;

/**
 * The crons controller
 *
 * @package     Aesir_Sync.Front
 * @subpackage  Controller.Crons
 * @since       1.0.0
 */
class Aesir_SyncControllerSync extends RControllerAdmin
{
	public function run() {
		$app = JFactory::getApplication();	
		$input = $app->input->getArray($_GET); // sanitize, escape htmlspecialchars, validate
		$input = array_map(function($a) {
			return htmlspecialchars(str_replace("'","",str_replace("\"","",$a)));
		},$input);
		$name = $input["method"];
		$temp = Sync::$name($input);
		echo json_encode($temp);
		$app->close();
	}




}
