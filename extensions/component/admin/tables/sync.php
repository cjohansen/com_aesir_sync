<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Tables.Sync
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die;

/**
 * Sync table.
 *
 * @package     Aesir_Sync.Backend
 * @subpackage  Tables.Sync
 * @since       1.0.0
 */
class Aesir_SyncTableSync extends RTable
{
	/**
	 * The table name without the prefix.
	 *
	 * @var  string
	 */
	protected $_tableName = 'aesir_sync_sync';
}
