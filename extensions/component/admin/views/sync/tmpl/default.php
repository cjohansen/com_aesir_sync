<?php
/**
 * @package     Aesir_Sync.Frontend
 * @subpackage  View.Crons
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die;

JHtml::_('rjquery.chosen', 'select');
JHtml::_('script', 'system/core.js', false, true);
JHtml::_('behavior.keepalive');


// -----------------------------------------------------------------------------------------------------


JLoader::import('sync', JPATH_ADMINISTRATOR . '/components/com_aesir_sync/classes');
JLoader::import('xmlimport', JPATH_ADMINISTRATOR . '/components/com_aesir_sync/classes');

?>


<html>
  <head>
    <link rel="stylesheet" type="text/css" href="../media/com_aesir_sync/css/aesir_sync.css">  
  </head>
	<body>
		<div id="com_aesir_sync"></div>
	</body>
</html>

<script src="../media/com_aesir_sync/js/aesir_sync.js"></script>


