<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Aesir_Sync
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die;

$app   = JFactory::getApplication();
$user  = JFactory::getUser();
$input = $app->input;

// Access check.
if (!$user->authorise('core.manage', 'com_aesir_sync'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// Load redITEM Library
JLoader::import('reditem.library');

$controller = $input->getCmd('view', 'sync');

// Set the controller page
if (!file_exists(JPATH_COMPONENT . '/controllers/' . $controller . '.php'))
{
	$controller = 'aesir_sync';
	$input->set('view', 'sync');
}

//RHelperAsset::load('aesir_sync.css', 'com_aesir_sync');

$controller = JControllerLegacy::getInstance('Aesir_Sync');
$controller->execute($input->getCmd('task', ''));
$controller->redirect();
