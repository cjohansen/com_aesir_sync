<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Classes.Queue
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

//namespace AesirSync;

defined('_JEXEC') or die;

class Queue 
{
	public $elements = [];
	public $size = 0;
	public $head = 0;
	public $tail = 0;
	public $isFull = false;
	public $isEmpty = true;

	function __construct($size) 
	{
		$this->size = $size;
	}

	function push(&$element) 
	{
		if ( !$this->isFull ) 
		{
			$this->elements[$this->head] = $element;
			$this->isEmpty = false;
			$this->head = ( $this->head + 1 ) % $this->size;
			if ( $this->head == $this->tail ) 
			{
				$this->isFull = true;
			} 
		}
	}

	function pull() 
	{
		if ( !$this->isEmpty ) 
		{
			$this->isFull = false;
			$return = $this->elements[$this->tail];
			$this->elements[$this->tail] = "";
			$this->tail = ( $this->tail + 1 ) % $this->size;
			if ( $this->head == $this->tail ) 
			{
				$this->isEmpty = true;
			} 
			return $return;
		}
		else return null;
	}
}
