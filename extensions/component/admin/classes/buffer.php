<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Classes.Buffer
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

//namespace AesirSync;

defined('_JEXEC') or die;

class Buffer 
{
	public $elements = [];
	public $size;
	public $isEmpty = true;
	public $isFull = false;

	function __construct($size) 
	{
		$this->size = $size;
	}

	function insert($key,$element) 
	{
		if ( count($this->elements) < $this->size ) 
		{
			$this->elements[$key] = $element;
			$this->isEmpty = false;
			if ( count($this->elements) == $this->size ) $this->isFull = true;
			return true;
		}
		else {
			return false;
		}
	}

	function remove($key) {
		$element = $this->elements[$key];
		unset($this->elements[$key]);
		$this->isFull = false;
		if ( count($this->elements) == 0 ) $this->isEmpty = true;
		return $element;
	}

}
