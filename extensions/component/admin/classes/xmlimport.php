<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Classes.XMLImport
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

//namespace AesirSync;

defined('_JEXEC') or die;

JLoader::import('queue', JPATH_ADMINISTRATOR . '/components/com_aesir_sync/classes');
JLoader::import('buffer', JPATH_ADMINISTRATOR . '/components/com_aesir_sync/classes');
JLoader::import('filedownload', JPATH_ADMINISTRATOR . '/components/com_aesir_sync/classes');
JLoader::import('helper', JPATH_ADMINISTRATOR . '/components/com_aesir_sync/classes');
JLoader::import('model', JPATH_ADMINISTRATOR . '/components/com_aesir_sync/classes');


class XMLImport 
{
	public $setup;
	public $file_download;
	public $xmlDoc;
	public $known = [];
	public $id;
	public $row = [];
	public $dependency = [];
	public $structure_only = true;
	public $i = 0;
	public $structure = [];
	public $structure2 = [];
	//public $xml_root;
	public $data;
	public $main_count = 0;
	public $data_type = [];
	public $max_depth;
	public $progress = 0;
	public $depth_main;

	/**
	 * constructor for object
	 *
	 * @param   array  $setup  Source item id
	 *
	 */
	function __construct($id) 
	{
		$this->id = $id;
		$this->file_download = new FileDownload();
		if ($id) $this->makeSetupFromDB();	
		$row = Sync::runSQL("select * from #__aesir_sync_xml where id=".$this->id,"loadAssoc");

		$this->reference = $row["reference"];
		$this->depth_main = $row["depth_main"];

	}

	/**
	 * generate import structure to database
	 */
	function generate() {
		$method = "download".$this->setup["content_type"];
		$this->$method();
$f = fopen("hejsa.php","a"); fwrite($f,"\n\n<?php /*skrammel-".date("H:i:s",mktime())."*/\n"); fwrite($f,"*data*".var_export($this->data,true)); fclose($f);
		$this->createTable();
		$this->getStructure();
		if ($this->depth_main==2) $count = count(array_pop($this->data));
		else $count = count($this->data);
$f = fopen("hejsa.php","a"); fwrite($f,"\n\n<?php /*skrammel-".date("H:i:s",mktime())."*/\n"); fwrite($f,"*count*".var_export($count,true)); fclose($f);
		Sync::runSQL("update #__aesir_sync_xml set main_count=".$count.", max_depth=".$this->max_depth.", depth_main=".$this->depth_main." where id=".$this->id,"execute");
		$this->createStructureTable();
	}

	/**
	 * download data as xml
	 */
	private function downloadXML() {
		$this->data = $this->filter(json_decode(json_encode(simplexml_load_file($this->setup["url"])),true));
	}

	/**
	 * download data as json
	 */
	private function downloadJSON() {
		$json = file_get_contents($this->setup["url"]);
		$this->data = json_decode($json,TRUE);
	}

	/**
	 * download data as csv
	 */
	private function downloadCSV() {
		// find separator used
		$csv = explode("\r",file_get_contents($this->setup["url"]));
		foreach([",",";","\t"] as $sep) {
			$headers = explode($sep,$csv[0]);
			$count0 = count($headers);
			if ( $count0 > 1 && $count0 == count(explode($sep,$csv[1])) ) break;
		}

		// use columns as keys in array
		for ($a = 1; $a < count($csv); $a++) {
			$line = [];
			$csv_line = explode($sep,$csv[$a]);
			for ($b = 0; $b < $count0; $b++ ) {
				$line[$headers[$b]] = $csv_line[$b];
			}
			$this->data[] = $line;
		}
	}

	/**
	 * if one element is prcessed insert that row
	 */
	private function insertRow() 
	{
		$item_default = array(
	    'title'         => "***",
	    'type_id'       => 14,
	    'alias'         => "***",
	    'category_id'   => null,
	    'categories'    => [],
	    'custom_fields' => [],
	    'params'        => [], // originalt: json_encode([]),  -eller- "[]"
	    'locations'     => []		
	  );

		$t = Aesir\Core\Helper\TableFinder::findAdmin('Item', 'com_reditem');
		$this->row = array_merge($item_default,$this->row);
		$this->row["title"] = $this->row["custom_fields"][$this->translate($this->cleanKey($this->setup["identifier"]))];
		$this->row["alias"] = $this->row["title"];
		$this->row["type_id"] = $this->reference;
		$t->save($this->row); // reditem_items, reditem_types, reditem_types_...., 
		//print_r($t->getErrors());

		Sync::runSQL("update #__aesir_sync_xml set progress=".(++$this->progress)." where id=".$this->id,"execute");
	}

	/**
	 * loop for loading not finsihed ressources
	 */
	private function translate($key) 
	{
		if ($this->setup["map"][$key]) return $this->setup["map"][$key];
		else return $key; 
	}

	/**
	 * add key/value pair to row
	 *
	 * @param   array  $setup  Source item id
	 *
	 */
	private function insertData( $name, $value ) 
	{ 
		$this->row["custom_fields"][$name] = $value;
	}
  
	/**
	 * loop for loading not finsihed ressources
	 */
	private function waitForLastResources() 
	{
		while (true)
		{
			if ( $this->file_download->clock() ) break;
		}
	}

	/**
	 * add field to aesir
	 *
	 * @param   array  $setup  Source item id
	 *
	 */
	private function addField($name) 
	{
//$f = fopen("hejsa.php","a"); fwrite($f,"\n\n<?php ".date("H:i:s",mktime())."*/\n"); fwrite($f,var_export($name,true)); fclose($f);			
		$field = array (
		  'types' => array ('0'=>'','1'=>'***'),
		  'name' => '***',
		  'type' => 'text',
		  'default' => '',
		  'is_filterable' => '0',
		  'version_note' => '',
		  'field_group_id' => '',
		  'relevance' => '0',
		  'params' => 
		  array (
		    'visible_frontend' => '1',
		    'enable_tooltip' => '1',
		    'tooltip' => '',
		    'note' => '',
		    'translatable' => '0',
		    'autocomplete' => '0',
		    'readonly' => '0',
		    'control' => 'hue',
		    'position' => 'bottom left',
		    'class' => '',
		  ),
		  'published' => '1',
		  'display_in_frontend_items_list_edit_view' => '0',
		  'display_in_frontend_item_edit_view' => '0',
		  'id' => '0',
		  'fieldcode' => '',
		);

		if ( !Sync::runSQL("select id from #__reditem_fields where name='".$name."'","loadResult") ) {
			$field["name"] = $name;
			$field["types"][1] = $this->reference;
			//if ($this->date_type[$name]) $field["type"] = "text";
			$t = Aesir\Core\Helper\TableFinder::findAdmin('Field', 'com_reditem');
			$t->save($field);
		}
	}

	/**
	 * create table in aesir
	 */
	private function createTable() 
	{
		$reference = Sync::runSQL("select reference from #__aesir_sync_xml where id=".$this->id,"loadResult");
		if (!$reference) {
			$t = Aesir\Core\Helper\TableFinder::findAdmin('Type', 'com_reditem');
			$t->bind( array('title' => $this->setup["name"], 'parent_id' => '1') );
			$t->save();
			$this->reference = $t->id;
			Sync::runSQL("update #__aesir_sync_xml set reference=".$this->reference." where id=".$this->id,"execute");
		}
		else {
			$this->reference = $reference;
		}
	}

	/**
	 * setup xml processor
	 *
	 * @param   array  $setup  Source item id
	 *
	 */
	private function unfold($node) 
	{
		$temp = simplexml_load_string($this->xmlDoc->saveXML($node));
		$key = $this->setup["unfolds"][$node->nodeName][0];
		$value = $this->setup["unfolds"][$node->nodeName][1];
		return [ "key"=> (string) $temp->$key, "value" => (string) $temp->$value ];
	}

	/**
	 * data is a key/value pair. The key need to follow a specific layout
	 *
	 * @param   array  $setup  Source item id
	 *
	 */
	private function cleanKey($key) 
	{
		return str_replace(["æ","ø","å","."],["ae","o","aa",""],preg_replace('/[^a-z0-9_æøå.]/', '_', strtolower(trim($key)))); // strtolower(trim($key)); 	
	}

	/**
	 * get the skeleton from the input data
	 */
	private function getStructure() {
		$header = Sync::runSQL("select name from #__aesir_sync_xml where id=".$this->id,"loadResult");
		if(is_numeric(array_shift(array_keys($this->data)))) $this->data = [$header=>$this->data];
		$this->getStructure2($this->data,$this->structure,$this->structure2);
	}

	/**
	 * is the other half of getStructure
	 *
	 * @param   array  $setup  Source item id
	 *
	 */
	private function getStructure2($node, &$structure, &$structure2, $path = "", $depth = 0 ) 
	{
		if ($depth > $this->max_depth) $this->max_depth = $depth; 
		$node = $this->filter($node);		
		foreach ( $node as $key => $element ) 
		{
			$new_path = $path."/".$key;
			if (is_numeric($key)) 
			{
				$structure2[$path] = 2; // is array
				if ( $depth == 1 ) $this->depth_main = 1;
				else $this->depth_main = 2; 
				$this->getStructure2( $element, $structure, $structure2, $path );
			}
			else {
				$structure2[$new_path] = 1;
				$structure2[$key] = $new_path;
				$this->data_type[$new_path] = $this->getDataType($element);
				$this->getStructure2( $element, $structure[$key], $structure2, $new_path, $depth + 1 );
			}
		}
	}

	/**
	 * setup xml processor
	 *
	 * @param   array  $setup  Source item id
	 *
	 */
	private function getDataType($element) {
		return "text";
	}

	/**
	 * recreates object from database
	 */
	private function makeSetupFromDB()
	{
		$data = Sync::runSQL("select * from #__aesir_sync_xml where id='".$this->id."'","loadAssoc");
		$this->setup["name"] = $data["name"];
		$this->setup["url"] = $data["url"];
		$this->setup["content_type"] = $data["content_type"];

		$this->makeSetupFromDB2($this->id,$this->setup);
	}

	/**
	 * setup xml processor
	 *
	 * @param   array  $setup  Source item id
	 *
	 */
	private function makeSetupFromDB2($parent, &$setup)
	{
		if ( $parent != "" ) 
		{ 
			$data = Sync::readSetupDB($parent);
			foreach ( $data as $value ) 
			{
				$value["filter"] = Sync::readMethodsDB($value["id"]);
				foreach ( $value as $key2=>$value2 ) 
				{
					if ( !( $value2 == "" || !$value2 ) ) 
					{
						switch ($key2)
						{						
							case "ignore_this":
							case "download":
							case "filter":
							case "map": $setup[$key2][$value["name"]] = $value2;
								break;
							case "unfold": $setup["unfolds"][$value["name"]] = [Sync::readUnfoldDB($value["id"],"the_key"),Sync::readUnfoldDB($value["id"],"the_value")];
								break;
							case "identifier":	
							case "main": $setup[$key2] = $value["name"]; 
						}
					}
				}
				$this->makeSetupFromDB2($value["id"],$setup);
			}
		}
	}

	/**
	 * make strukture in database from the skeleton obtained from input data
	 */
	private function createStructureTable() {
		$this->createStructureTable2($this->structure,$this->id);
	}

	/**
	 * part of createStructureTable
	 *
	 * @param   array  $setup  Source item id
	 *
	 */
	private function createStructureTable2($structure, $parent = 0, $depth = 0) 
	{
		foreach ($structure as $key=>$value) 
		{
			//$new_parent = Sync::insertSetupDB($parent,substr($key,strrpos($key,"/")+1),$key);
			$new_parent = Sync::insertSetupDB($parent,$key,$key);
			if ( $value ) $this->createStructureTable2($structure[$key], $new_parent, $depth + 1);
		} 
	}

	/**
	 * load all data from input into database and begin processing data
	 */
	public function process() {
		$method = "download".$this->setup["content_type"];
		$this->$method();

		$this->getStructure();
$f = fopen("hejsa.php","a"); fwrite($f,"\n\n<?php /*".date("H:i:s",mktime())."*/\n"); fwrite($f,var_export($this->structure,true)); fclose($f);
$f = fopen("hejsa.php","a"); fwrite($f,"\n\n<?php /*".date("H:i:s",mktime())."*/\n"); fwrite($f,var_export($this->structure2,true)); fclose($f);
//$f = fopen("hejsa.php","a"); fwrite($f,"\n\n<?php /*skrammel-".date("H:i:s",mktime())."*/\n"); fwrite($f,"*data*".var_export($this->data,true)); fclose($f);
$f = fopen("hejsa.php","a"); fwrite($f,"\n\n<?php /*skrammel-".date("H:i:s",mktime())."*/\n"); fwrite($f,"*setup*".var_export($this->setup,true)); fclose($f);
		$this->p($this->data);		
		$this->waitForLastResources();
	}

	/**
	 * process input data
	 *
	 * @param   array  $setup  Source item id
	 *
	 */
	private function p( $array, $depth = 0, &$path2 = [], $path = "", &$main = null, $previous = "") 
	{
		$array = $this->filter($array);
		foreach ($array as $key => $value) 
		{
			$key = trim($key);
			$path2[$depth] = [$key,$value];
			$path3 = (is_numeric($key) ? $path : $path."/".$key);			
			if ( array_key_exists($path2[$depth-1][0], $this->setup["unfolds"]) ) 
			{
				$new_key = $value[$this->setup["unfolds"][$path2[$depth-1][0]][0]];
				$new_value = $value[$this->setup["unfolds"][$path2[$depth-1][0]][1]]; 
				if ( $this->structure2[$path3] != 2 )  $this->pN($new_key, $new_value, $depth, $path2, $path3, $this->setup["unfolds"][$path2[$depth-1][0]][0]);
			}
			else 
			{
				if ( $this->structure2[$path3] != 2 )  $this->pN($key,$value,$depth,$path2,$path3,$key);
				if ( !$this->setup["ignore_this"][$key] ) $this->p($value, $depth+1, $path2, $path3, $main, $test["hejsa"]);
			}
		}
		if ( $depth == 2 ) 
		{
			$t = $this->insertRow();
//$q = "depth_main:".$this->depth_main.", depth:".$depth;
//$f = fopen("hejsa.php","a"); fwrite($f,"\n\n<?php /*skrammel-".date("H:i:s",mktime())."*/\n"); fwrite($f,"*test*".var_export($,true)); fclose($f);
		}
		$this->file_download->clock();
	}

	/**
	 * process a note in input data
	 *
	 * @param   array  $setup  Source item id
	 *
	 */
	private function pN($key, $value, $depth, $path2, $path, $org_key ) 
	{
		if ( $this->setup["ignore_this"][$key] ) return;
		$key = $this->cleanKey($key);

		$name = ( $this->setup["map"][$key] ? $this->setup["map"][$key] : $key );
		if ( $this->setup["download"][$name] ) $this->file_download->download($value);
		if ( $this->setup["filter"][$org_key] ) 
		{
			foreach ( $this->setup["filter"][$org_key] as $func ) $value = Helper::$func($value,$path2,$depth);
		}
		if ( !$this->known[$name] ) 
		{
			$this->addField($name); 
			$this->known[$name] = 1;
		} 
		$this->insertData( $name, $value );
	}

	/**
	 * remove all additional data than the actual key/value
	 *
	 * @param   array  $setup  Source item id
	 *
	 */
	private function filter($array) {
		return array_filter($array, function($key) {return (substr($key,0,1)!="@");}, ARRAY_FILTER_USE_KEY);		
	}


}
