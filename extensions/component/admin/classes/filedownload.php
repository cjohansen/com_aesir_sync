<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Classes.FileDownload
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

//namespace AesirSync;

defined('_JEXEC') or die;

class FileDownload 
{
	public $download_buffer;
	public $image_queue;
	public $i = 0;
  public $multi;
  public $curls = array();
  public $options = array(	
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_MAXREDIRS => 5
  );
  public $buffer_size = 5;
  public $ratio = 10000;
  public $finished = false;

	function __construct() 
	{
		$this->download_buffer = new Buffer($this->buffer_size);
		$this->image_queue = new Queue(1000000);	
		$this->multi = curl_multi_init();
	}

	public function download($url) 
	{
		$this->image_queue->push($url);
	}

	public function clock() 
	{
		$this->queue_to_buffer();
		return $this->poke();
	}

	public function queue_to_buffer() 
	{
		if ( !$this->image_queue->isEmpty && !$this->download_buffer->isFull ) 
		{
			$url = $this->image_queue->pull();
		  $handle = curl_init($url);
		  curl_setopt_array($handle,$this->options);
		  $file = fopen(JPATH_BASE."/media/com_aesir_sync/ressourcer/".($this->i++).".".explode("?",pathinfo($url)["extension"])[0],"w");
		  $this->download_buffer->insert($handle,$file);
		  curl_setopt($handle, CURLOPT_FILE, $file);
		  curl_multi_add_handle($this->multi, $handle);
		}
	}

	private function remove_finished($handle)
	{
		$file = $this->download_buffer->remove($handle);
    fclose($file);
    curl_multi_remove_handle($this->multi, $handle);
	}

	private function poke() {
	  for ( $a = 0; $a < $this->ratio; $a++ ) {
	    $execrun = curl_multi_exec($this->multi, $running);
	    if($execrun != CURLM_OK) return -1;
	    else if (!$running) {
		    return 1;
	    } 
	    if ($done = curl_multi_info_read($this->multi)) {
	      $info = curl_getinfo($done['handle']);
	      if ($info['http_code'] == 200) $this->remove_finished($done["handle"]);
	      else return -2;
	    }
	  }   
	  return 0; 
	}

}
