<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Classes.Helper
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

//namespace AesirSync;

defined('_JEXEC') or die;

class Helper 
{

	static function reverse($data,$path,$depth) 
	{
		return strrev($data);
	}

	static function upper($data,$path,$depth) 
	{
		return strtoupper($data);
	}

	static function upperMedDato($data,$path,$depth) 
	{
		return strtoupper($data)."(dato=".$path[$depth-1][1]["date"].")";
	}

}
