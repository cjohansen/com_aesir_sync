<?php
/**
 * @package     Aesir_Sync.Backend
 * @subpackage  Classes.Sync
 *
 * @copyright   Copyright (C) 2012 - 2017 redCOMPONENT.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

//namespace AesirSync;

defined('_JEXEC') or die;

JLoader::import('xmlimport', JPATH_ADMINISTRATOR . '/components/com_aesir_sync/classes');
JLoader::import('model', JPATH_ADMINISTRATOR . '/components/com_aesir_sync/classes');

class Sync
{
	//public static $ref;
	public $sources;
	private $id;

	function __construct() 
	{
		//self::$ref = $this;
		$this->id = time();

		$this->sources = self::getSynchronize();

		self::runSQL("insert into #__aesir_sync_statistics(setup_id,begin) values(".$this->id.",'".date("Y-m-d H:i:s",time())."')","execute");
		$this->synchronize();
		self::runSQL("update #__aesir_sync_statistics set end='".date("Y-m-d H:i:s",time())."' where setup_id=".$this->id,"execute");
	}	

	public function synchronize() 
	{
		$this->synchronize2($this->sources);
	}

	private function synchronize2($node) 
	{
		foreach($node as $key => $value) 
		{
			if ($value["type"]!="root") (new XMLImport($key))->process();
			$this->synchronize2($value["children"]);
		}
	}

	// ------------------------ model ---------------------------------

	/**
	 * insert new link to external data
	 */
	public static function insertSetupDB($parent,$name,$path) 
	{
		return self::runSQL("insert into #__aesir_sync_xml(parent_id,name,type,path,datatype) values(".$parent.",'".$name."','xml','".$path."','text')","insertid");
	}

	/**
	 * read info for on elink to external data
	 */
	public static function readSetupDB($parent)
	{
		return self::runSQL("select * from #__aesir_sync_xml where parent_id=".$parent,"loadAssocList");
	}

	/**
	 * read key/value pair related to specific element
	 */
	public static function readUnfoldDB($parent, $key_value) {
		return self::runSQL("select name from #__aesir_sync_xml where parent_id=".$parent." and ".$key_value."=1","loadResult");
	}

	/**
	 * read methods for specfic element
	 */
	public static function readMethodsDB($id) {
		return self::runSQL(	"select name from #__aesir_sync_setup_methods as amsm ".
													"join #__aesir_sync_methods as amm ".
													"on amsm.method_id=amm.id ".
													"where setup_id=".$id." order by amsm.sequence_number","loadColumn");
	}

	/**
	 * get all methods available
	 */
	public static function readAllMethodsDB() {
		return self::runSQL("select name from #__aesir_sync_methods","loadColumn");
	}

	/**
	 * wrap sql 
	 */
	public static function runSQL($query,$method) {
		$db = JFactory::getDbo();
		$db->setQuery($query);
		if ($method=="insertid") $db->execute(); 
		return $db->$method();
	}

	/**
	 * get tree only containing marked as synchronize
	 */
	public static function getSynchronize() {
		return self::getTree(null,1);
	}

	/**
	 * get a subtree out of maintree
	 */
	public static function getBranch($id) {
		return [$id=>["id"=>$id,"children"=>self::getTree(null,0,$id)]];
	}

	/**
	 * get complete tree
	 */
	public static function getTree($input,$sync = 0,$id = 0) {
		$sql = 	"select ". 
						"*, ". 
						"(select GROUP_CONCAT(name) from #__aesir_sync_methods as t3 ".
						"join #__aesir_sync_setup_methods as t4 on t4.method_id=t3.id ".
						"where t4.setup_id=t1.id) as methods ".
						"from #__aesir_sync_xml as t1";
		if ( $sync ) $sql .= " where t1.sync=1 or t1.parent_id=0";

		$all = self::runSQL($sql,"loadAssocList");		
		foreach($all as $value) {
			$all_by_parent[$value["parent_id"]][] = $value;
		}
		self::getTree2($all_by_parent[$id],$all_by_parent,$tree);
		return $tree;
	}

	/**
	 * part of getTree
	 */
	public static function getTree2($elements,&$elements_org,&$tree) {
		foreach ($elements as $key => $value) 
		{
			if ($elements_org[$value["id"]]) $value["children"] = [];
			$tree[$value["id"]] = $value;
			if ($elements_org[$value["id"]]) self::getTree2($elements_org[$value["id"]],$elements_org,$tree[$value["id"]]["children"]);
		}

	} 

	/**
	 * delete element and subelements
	 */
	public static function deleteXML($input)
	{
		$a = self::runSQL("select reference, name from #__aesir_sync_xml where id=".$input["id"],"loadAssoc");
		self::deleteXMl_2(self::getBranch($input["id"]));
		self::runSQL("delete from #__reditem_types where id=".$a["reference"],"execute");
		self::runSQL("drop table #__reditem_types_".$a["name"]."_".$a["reference"],"execute");
		self::runSQL("delete from #__reditem_items where type_id=".$a["reference"],"execute");
	}

	/**
	 * part of deleteXML
	 */
	public static function deleteXML_2($node) {
		foreach($node as $key => $value) {
			self::runSQL("delete from #__aesir_sync_xml where id=".$value["id"],"execute");
			self::deleteXML_2($value["children"]);
		}
	}

	/**
	 * insert new endpoint
	 */
	public static function addXML($input)
	{
		self::runSQL("insert into #__aesir_sync_xml(parent_id,name,type,url,content_type) values(".$input["id"].",'new XML','url','http://address_to_xml','xml') ","execute");
		return self::getTree();
	}

	/**
	 * set methods on specific element
	 */
	public static function setMethods($input)
	{
		self::runSQL("delete from #__aesir_sync_setup_methods where setup_id=".$input["id"],"execute");
		foreach(explode(",",$input["value"]) as $value) {
			self::runSQL("insert into #__aesir_sync_setup_methods(setup_id,method_id) ".
				"values(".$input["id"].",(select id from #__aesir_sync_methods where name='".$value."'))","execute");
		}
	}

	/**
	 * set option on specific element (option fx. ignore, download, unfold)
	 */
	public static function setOption($input)
	{
		return self::runSQL("update #__aesir_sync_xml set ".$input["key"]."='".$input["value"]."' where id=".$input["id"],"execute");
	}

	/**
	 * update data on endpoint and read/analyze data
	 */
	public static function loadXML($input) 
	{
		self::runSQL("update #__aesir_sync_xml set name='".$input["name"]."', url='".base64_decode($input["url"])."', ".
									"content_type='".$input["content_type"]."' where id=".$input["id"],"execute");
		$t = new XMLImport($input["id"]);
		$t->generate();
		return self::getTree();
	}

	/**
	 * sync elements marked as synchronize immidiately
	 */
	public static function syncAll($input) 
	{

		$output = shell_exec("php -c /Applications/MAMP/bin/php/php7.0.13/conf/php.ini ".JPATH_ROOT."/cli/com_aesir_sync/cron.php");
		return $output;
	}

	/**
	 * split timestamp into day, hour, seconds ......
	 */
	public static function splitDateTime($datetime,$substitute) 
	{
		$output = [];
		$t = new DateTime($datetime);
		foreach (["H","i","m","d","w","D","N"] as $value) $output[$value] = $t->format($value);			
		$output = array_merge($output,$substitute);
		return $output;
	}

	/**
	 * add a job to cron
	 */
	public static function addCron($input) 
	{
		if ($input["datetime_repeat"]=="repeat same day every month") $t = self::splitDateTime($input["datetime"],["m"=>"*","N"=>"*"]);
		//else if ($input["datetime_repeat"]=="just once") $t = self::splitDateTime($input["datetime"],["m"=>"*","d"=>"*"]);
		else if ($input["datetime_repeat"]=="repeat same day every week") $t = self::splitDateTime($input["datetime"],["m"=>"*","d"=>"*"]);

		$w = ($t["N"]!="*" ? ($t["N"]%7) : "*");
		$output = shell_exec(	"(crontab -l 2>/dev/null; echo '".$t["i"]." ".$t["H"]." ".$t["d"]." ".$t["m"]." ".$w." ".
													"php ".JPATH_BASE."/cli/com_aesir_sync/cron.php') | crontab -");
		return $output;
	}

	/**
	 * remove cronjob
	 */
	public static function removeCron($input) 
	{
		$output = shell_exec("crontab -l | grep -v 'php ".JPATH_BASE."/cli/com_aesir_sync/cron.php' | crontab -");
		return $output;
	}

	/**
	 * get synchronize progress on specific element
	 */
	public static function getProgress($input) 
	{
		return self::runSQL("select (progress/main_count) as percent, running, id from #__aesir_sync_xml where id=".$input["id"],"loadAssoc");
	}

// ----------------------- test -------------------------------------	

	public static function test($input) {

		//$t = new XMLImport($input["id"]);

		/*$method = "download".$t->setup["content_type"];
		$t->$method();

		$t->getStructure();*/
		//return shell_exec("ls");
	}


}
