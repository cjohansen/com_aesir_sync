SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;


-- -----------------------------------------------------
-- Table `#__aesir_sync_xml`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__aesir_sync_xml` ;

CREATE TABLE IF NOT EXISTS `#__aesir_sync_xml` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `type` varchar(40) NOT NULL,
  `link_id` int(11) NOT NULL,
  `path` varchar(400) NOT NULL,
  `name` varchar(40) NOT NULL,
  `url` varchar(200) NOT NULL,
  `download` tinyint(4) NOT NULL,
  `unfold` tinyint(4) NOT NULL,
  `not_overwrite` tinyint(4) NOT NULL,
  `the_key` tinyint(4) NOT NULL,
  `the_value` tinyint(4) NOT NULL,
  `unfold_key` varchar(40) NOT NULL,
  `unfold_value` varchar(40) NOT NULL,
  `main` tinyint(4) NOT NULL,
  `map` varchar(40) NOT NULL,
  `ignore_this` tinyint(4) NOT NULL,
  `collapsed` tinyint(4) NOT NULL,
  `sync` tinyint(4) NOT NULL,
  `sync_last` timestamp NULL DEFAULT NULL,
  `table_created` tinyint(4) NOT NULL,
  `content_type` varchar(20) NOT NULL,
  `reference` int(11) NOT NULL,
  `main_count` int(11) NOT NULL,
  `cron` varchar(200) NOT NULL,
  `latest_download` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `datatype` varchar(20) NOT NULL,
  `identifier` tinyint(4) NOT NULL,
  `max_depth` int(11) NOT NULL,
  `progress` int(11) NOT NULL,
  `depth_main` int(11) NOT NULL,
  `running` tinyint(4) NOT NULL
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `#__aesir_sync_xml`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `#__aesir_sync_xml`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8261;


-- -----------------------------------------------------
-- Table `#__aesir_sync_sync`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__aesir_sync_methods` ;

CREATE TABLE IF NOT EXISTS `#__aesir_sync_methods` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `#__aesir_sync_methods`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `#__aesir_sync_methods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;


-- -----------------------------------------------------
-- Table `#__aesir_sync_setup_methods`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__aesir_sync_setup_methods` ;

CREATE TABLE IF NOT EXISTS `#__aesir_sync_setup_methods` (
  `id` int(11) NOT NULL,
  `setup_id` int(11) NOT NULL,
  `method_id` int(11) NOT NULL,
  `sequence_number` int(11) NOT NULL
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `#__aesir_sync_setup_methods`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `#__aesir_sync_setup_methods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;



-- -----------------------------------------------------
-- Table `#__aesir_sync_methods`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `#__aesir_sync_statistics` ;

CREATE TABLE `#__aesir_sync_statistics` (
  `id` int(11) NOT NULL,
  `setup_id` int(11) NOT NULL,
  `begin` timestamp NULL DEFAULT NULL,
  `end` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `#__aesir_sync_statistics`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `#__aesir_sync_statistics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;




SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
