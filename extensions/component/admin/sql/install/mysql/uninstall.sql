SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `#__aesir_sync_cron`;
DROP TABLE IF EXISTS `#__aesir_sync_sync`;
DROP TABLE IF EXISTS `#__aesir_sync_config` ;

SET FOREIGN_KEY_CHECKS = 1;
